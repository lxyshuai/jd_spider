# -*- coding: utf-8 -*-
import json
import requests
import scrapy
import xlrd
import re

from scrapy.spiders import Spider
from jd_spider.items import CommentItem

"""
网页url传递了8个参数，参数描述如下：

callback：参数由```'fetchJSON_comment98vv' + commentVersion`构成，commentVersion可以在商品信息的html代码中通过正则表达式获取

commentVersion

productId：商品的ID，如3995645

score：表示商品的评论栏
全部评价：0
好评：3
中评：2
差评：1
追评：5
晒图：4

page：表示当前是第几页评论

sortType：排序方式
时间排序：6
推荐排序：5

pageSize：每页的评论数量，默认是10个，只能小于等于10

isShadowSku：不知道，默认为0

fold：不知道，默认为1
"""
PRODUCT_URL = 'https://item.jd.com/{product_id}.html'
PRODUCT_COMMENT_URL = 'https://club.jd.com/comment/skuProductPageComments.action' \
                      '?callback=fetchJSON_comment98vv{comment_version}' \
                      '&productId={product_id}' \
                      '&score={score}' \
                      '&sortType={sort_type}' \
                      '&page={page}' \
                      '&pageSize=10' \
                      '&isShadowSku=0' \
                      '&fold=1'
ALL_COMMENT_SCORE_INDEX = 0
BAD_COMMENT_SCORE_INDEX = 1
GOOD_COMMENT_SCORE_INDEX = 3
BAD_COMMENT_SCORE = 1
GOOD_COMMENT_SCORE = 5




class CommentSpider(Spider):
    name = "comment"

    def start_requests(self):
        xlrd.Book.encoding = "utf-8"
        data = xlrd.open_workbook("products.xls")
        # goods为要抓取评论的商品信息，现提供一个goods.xls文件供参考,第1列：商品ID
        table = data.sheets()[0]
        # 商品ID list
        product_id_list = table.col_values(0)

        # 一件商品一件商品的抽取
        for product_id in product_id_list:
            # 好评3，中评2，差评1
            for score in (GOOD_COMMENT_SCORE_INDEX, BAD_COMMENT_SCORE_INDEX):
                # 一页一页抽取
                for page in range(30):
                    yield scrapy.Request(url=CommentSpider.generate_product_comment_url(product_id=int(product_id),
                                                                                        score=score,
                                                                                        sort_type=6,
                                                                                        page=page), callback=self.parse)

    def parse(self, response):
        if response.text:
            temp = response.text.split('productAttr')
            comments_json = '{"productAttr' + temp[1][:-2]

            comments_dict = json.loads(comments_json, strict=False)
            comments = comments_dict['comments']  # 该页所有评论
        items = []
        try:
            for comment in comments:
                if comment['score'] in (BAD_COMMENT_SCORE, GOOD_COMMENT_SCORE):
                    item = CommentItem()
                    item['product_id'] = comment['referenceId']
                    item['product_comment_score'] = comment['score']
                    item['product_comment_content'] = comment['content']
                    items.append(item)
        except:
            print 1
        return items

    @staticmethod
    def get_comment_version(product_id):
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
        }
        product_url = PRODUCT_URL.format(product_id=product_id)
        response = requests.get(url=product_url, headers=headers)
        pattern = re.compile(r"commentVersion:'(.*?)'")
        comment_version = re.search(pattern, response.text).group(1)
        return comment_version

    @staticmethod
    def generate_product_comment_url(product_id, score, sort_type, page):
        comment_version = CommentSpider.get_comment_version(product_id)
        product_comment_url = PRODUCT_COMMENT_URL.format(comment_version=comment_version, product_id=product_id,
                                                         score=score, sort_type=sort_type, page=page)
        return product_comment_url
